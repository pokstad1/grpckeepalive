package grpckeepalive_test

import (
	"context"
	"net"
	"testing"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/examples/helloworld/helloworld"
	"google.golang.org/grpc/keepalive"
)

func TestKeepalive(t *testing.T) {
	s := grpc.NewServer(
		grpc.KeepaliveEnforcementPolicy(
			keepalive.EnforcementPolicy{
				MinTime:             10 * time.Second,
				PermitWithoutStream: true,
			},
		),
	)
	defer s.GracefulStop()

	helloworld.RegisterGreeterServer(s, GreeterServer{})

	lis, err := net.Listen("tcp", "localhost:6060")
	if err != nil {
		t.Fatal(err)
	}
	defer lis.Close()

	errQ := make(chan error)
	go func() {
		errQ <- s.Serve(lis)
	}()

	cc, err := grpc.Dial("localhost:6060",
		grpc.WithBlock(),
		grpc.WithInsecure(),
	)
	if err != nil {
		t.Fatal(err)
	}
	defer cc.Close()

	select {
	case err := <-errQ:
		t.Fatal(err)
	case <-time.After(30 * time.Second):
		return
	}
}

type GreeterServer struct{}

func (gs GreeterServer) SayHello(_ context.Context, _ *helloworld.HelloRequest) (*helloworld.HelloReply, error) {
	return &helloworld.HelloReply{}, nil
}
